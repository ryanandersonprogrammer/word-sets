# Word Sets Module

## About

### Author

Ryan E. Anderson

---

### Description

Word Sets Module contains functions that can be used to evaluate sets of words.

- Use predicates to retrieve information about words.
- Retrieve sets that are filtered by a letter and an index.

---

### Version

1.0.0

---

### License

Apache-2.0

---

## get_word_length_subset(predicate_membership, list_word_superset, list_word_set)

Retrieve a set containing lengths that were qualified as members by the use of a given predicate.

This function will return a subset of a superset of lengths that was formed using a provided list of words. If a subset 
cannot be determined, then an empty set will be returned.

### Parameters

#### predicate_membership

This parameter accepts either a lambda expression or a function that will determine whether lengths of words are members 
of a set.

#### list_word_superset

This parameter accepts a list of words that will be used to form a superset of lengths.

#### list_word_set

This parameter accepts a list of words that will be used to form a set of lengths by using a given predicate.

### Output

This function will return either an empty set or a subset of a superset of lengths.

## get_set_of_words_that_start_with_letter(letter, index, *args)

Retrieve a set of words that contain a target letter at a specified index.

### Parameters

#### letter

Elements from provided sets will be filtered based on this parameter, which represents a target letter that is located 
at a specified index.

#### index

This parameter represents the location of a target letter.

#### *args

This parameter allows a variable number of sets to be passed as arguments so that separate collections of words can be 
filtered together.

### Output

A set containing all of the words that have a given letter at a specified index will be returned.

## Using Word Sets Module

Below is a sample script that demonstrates how to import Word Sets Module and use a function to get information about 
words.

```python
import wordsets

list_superset = ["and", "four", "two", "world"]
list_set = ["a", "to", "three"]

subset_greater_than_two = wordsets.get_word_length_subset(lambda w: w > 2, list_superset, list_set)

print(subset_greater_than_two)
```

Word Sets Module can also be used in a Python shell.

## Demo

A module and a script are provided to evaluate the functionality of Word Sets Module.

## Tests

Unit tests are provided to evaluate Word Sets Module. Mocking was used to get maximal coverage of a function.