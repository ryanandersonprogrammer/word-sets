# Copyright 2019 Ryan E. Anderson
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Word Sets Demo Module v1.0.0
#
# by Ryan E. Anderson
#
# Copyright (C) 2019 Ryan E. Anderson

"""Word Sets Demo Module

This module contains functions that can be used to evaluate Word Sets Module.
"""


def append_input_to_list_of_set_elements(size_set, list_elements):
    """Retrieve a list of elements that can be used to create a set.

    Prompt a user to enter values that will be appended to a list.

    Parameters
    ----------
    size_set : int
        This is the cardinality of a set.
    list_elements : list
        Elements will be appended to this list.
    """
    for i in range(0, size_set):
        element = input(f"Element {i+1}: ")

        list_elements.append(element)

    print("")
    print(f"The list {list_elements} was created.")
    print("")


def get_difference_between_subset_and_superset(superset, subset):
    """Retrieve the difference between a superset and a subset.

    Parameters
    ----------
    superset : set
        This a superset of elements.
    subset : set
        This is a subset of a provided superset.

    Returns
    -------
    set
        A collection of all of the elements that are not in the subset will be returned if the conditions for
        containment are satisfied; otherwise, an empty set will be returned.

    Raises
    ------
    TypeError
        This error is raised when the superset is not a set.
        This error is raised when the subset is not a set.
    """
    if not isinstance(superset, set):
        raise TypeError("The argument that was passed to superset is not a set.")

    if not isinstance(subset, set):
        raise TypeError("The argument that was passed to subset is not a set.")

    return set() if not subset.issubset(superset) else superset.difference(subset)


def is_less_than_four(value):
    """Determine whether a given value is less than four.

    Parameters
    ----------
    value : int
        Any valid argument that is passed to this parameter will be used in a comparison to determine whether it is less
        than four.

    Returns
    -------
    bool
        If the provided value is less than four, then true will be returned; otherwise, this function will return false.

    Raises
    ------
    TypeError
        This error is raised when value is not an integer.
    """
    if not isinstance(value, int):
        raise TypeError("The argument that was passed to value is not an integer.")

    return value < 4
