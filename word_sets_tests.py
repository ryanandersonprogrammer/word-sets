# Copyright 2019 Ryan E. Anderson
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Word Sets Module Tests v1.0.0
#
# by Ryan E. Anderson
#
# Copyright (C) 2019 Ryan E. Anderson

"""Word Sets Module Tests

This module contains classes that can be used to test the functions of Word Sets Module.
"""
from unittest import TestCase, mock, main
from unittest.mock import Mock
from types import FunctionType, LambdaType

import wordsets


class TestWordSetsGetWordLengthSubset(TestCase):
    """This class can be used to evaluate get_word_length_subset."""

    def setUp(self):
        def is_even(w):
            return w % 2 == 0

        self.list_word_superset = ["one", "four", "three"]
        self.membership_lambda = lambda w: w % 2 == 0
        self.membership_function = is_even

    def test_an_empty_set_is_returned_when_a_provided_set_does_not_contain_a_word_that_has_four_characters(self):
        subset_lambda = wordsets.get_word_length_subset(self.membership_lambda, self.list_word_superset,
                                                        ["is", "not", "a"])
        subset_function = wordsets.get_word_length_subset(self.membership_function, self.list_word_superset,
                                                          ["is", "not", "a"])

        self.assertSetEqual(set(), subset_lambda)
        self.assertSetEqual(set(), subset_function)

    def test_an_empty_set_is_returned_when_a_provided_set_contains_two_words_that_have_an_even_length(self):
        subset_lambda = wordsets.get_word_length_subset(self.membership_lambda, self.list_word_superset,
                                                        ["is", "true", "a"])
        subset_function = wordsets.get_word_length_subset(self.membership_function, self.list_word_superset,
                                                          ["is", "true", "a"])

        self.assertSetEqual(set(), subset_lambda)
        self.assertSetEqual(set(), subset_function)

    def test_a_set_with_one_element_is_returned_when_a_provided_set_contains_a_word_that_has_four_characters(self):
        subset_lambda = wordsets.get_word_length_subset(self.membership_lambda, self.list_word_superset, ["true", "a"])
        subset_function = wordsets.get_word_length_subset(self.membership_function, self.list_word_superset,
                                                          ["true", "a"])

        self.assertSetEqual({4}, subset_lambda)
        self.assertSetEqual({4}, subset_function)

    def test_type_error_is_raised_when_the_predicate_is_neither_a_function_nor_a_lambda_expression(self):
        self.assertRaises(TypeError, wordsets.get_word_length_subset, 1, self.list_word_superset, ["true", "a"])

    def test_predicate_membership_has_two_calls_when_provided_set_contains_two_elements(self):
        list_word_set = ["a", "an"]

        mock_predicate_membership_function = Mock(FunctionType)
        mock_predicate_membership_lambda = Mock(LambdaType)
        mock_predicate_membership_function.side_effect = [False, True]
        mock_predicate_membership_lambda.side_effect = [False, True]

        subset_function = wordsets.get_word_length_subset(mock_predicate_membership_function, ["four", "to", "three"],
                                                          list_word_set)
        subset_lambda = wordsets.get_word_length_subset(mock_predicate_membership_lambda, ["four", "to", "three"],
                                                        list_word_set)

        self.assertSetEqual({2}, subset_function)
        self.assertSetEqual({2}, subset_lambda)
        self.assertTrue(mock_predicate_membership_function.call_count == 2)
        self.assertTrue(mock_predicate_membership_lambda.call_count == 2)

        mock_predicate_membership_function.assert_has_calls(
            [mock.call(len(list_word_set[0])), mock.call(len(list_word_set[1]))])
        mock_predicate_membership_lambda.assert_has_calls(
            [mock.call(len(list_word_set[0])), mock.call(len(list_word_set[1]))])


class TestWordSetsGetSetOfWordsThatStartWithLetter(TestCase):
    """This class can be used to evaluate get_set_of_words_that_start_with_letter."""

    def test_a_set_with_three_members_is_returned_when_each_set_contains_a_word_that_has_the_target_letter(self):
        set_first = {"is", "not", "an"}
        set_second = {"any", "of", "these"}
        set_third = {"four", "to", "are"}

        set_of_words_that_start_with_letter = wordsets.get_set_of_words_that_start_with_letter("a", 0, set_first,
                                                                                               set_second, set_third)

        self.assertSetEqual({"an", "any", "are"}, set_of_words_that_start_with_letter)

    def test_a_set_with_two_members_is_returned_when_the_length_of_a_word_is_less_than_the_index(self):
        set_words = {"ant", "antenna", "a"}

        set_of_words_that_start_with_letter = wordsets.get_set_of_words_that_start_with_letter("t", 2, set_words)

        self.assertSetEqual({"ant", "antenna"}, set_of_words_that_start_with_letter)

    def test_a_set_with_two_members_is_returned_when_the_length_of_a_word_is_equal_to_the_index(self):
        set_words = {"ant", "antenna", "an"}

        set_of_words_that_start_with_letter = wordsets.get_set_of_words_that_start_with_letter("t", 2, set_words)

        self.assertSetEqual({"ant", "antenna"}, set_of_words_that_start_with_letter)

    def test_type_error_is_raised_when_letter_is_not_a_string(self):
        self.assertRaises(TypeError, wordsets.get_set_of_words_that_start_with_letter, 1, 0, {"a", "four", "two"})

    def test_value_error_is_raised_when_letter_is_a_string_that_does_not_have_a_length_of_one(self):
        letter = "two"

        self.assertTrue(isinstance(letter, str))
        self.assertRaises(ValueError, wordsets.get_set_of_words_that_start_with_letter, letter, 0, {"a", "four", "two"})

    def test_type_error_is_raised_when_index_is_not_an_integer(self):
        self.assertRaises(TypeError, wordsets.get_set_of_words_that_start_with_letter, "a", "a", {"a", "four", "two"})

    def test_value_error_is_raised_when_index_is_an_integer_that_is_less_than_zero(self):
        index = -1

        self.assertTrue(isinstance(index, int))
        self.assertRaises(ValueError, wordsets.get_set_of_words_that_start_with_letter, "a", index,
                          {"a", "four", "two"})

    def test_type_error_is_raised_when_a_list_is_passed_as_an_argument(self):
        self.assertRaises(TypeError, wordsets.get_set_of_words_that_start_with_letter, "a", 0, {"a", "four", "two"},
                          ["a", "b"])


if __name__ == '__main__':
    main()
