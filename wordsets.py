# Copyright 2019 Ryan E. Anderson
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Word Sets Module v1.0.0
#
# by Ryan E. Anderson
#
# Copyright (C) 2019 Ryan E. Anderson

"""Word Sets Module

This module contains functions for evaluating sets of words.
"""
import types


def get_word_length_subset(predicate_membership, list_word_superset, list_word_set):
    """Retrieve a set containing lengths that were qualified as members by the use of a given predicate.

    This function will return a subset of a superset of lengths that was formed using a provided list of words. If a
    subset cannot be determined, then an empty set will be returned.

    Parameters
    ----------
    predicate_membership : function
        This parameter accepts either a lambda expression or a function that will determine whether lengths of words are
        members of a set.
    list_word_superset : list
        This parameter accepts a list of words that will be used to form a superset of lengths.
    list_word_set : list
        This parameter accepts a list of words that will be used to form a set of lengths by using a given predicate.

    Returns
    -------
    set
        This function will return either an empty set or a subset of a superset of lengths.

    Raises
    ------
    TypeError
        This error is raised when the predicate for membership is neither a function nor a lambda expression.
    """
    if not isinstance(predicate_membership, types.FunctionType):
        raise TypeError("The predicate for membership must be a function or a lambda expression.")

    list_word_lengths_superset = map(lambda w: len(w), list_word_superset)
    list_word_lengths = map(lambda w: len(w), list_word_set)

    set_word_lengths_superset = set(list_word_lengths_superset)
    set_word_lengths = set(list_word_lengths)

    set_word_lengths_subset = set(w for w in set_word_lengths if predicate_membership(w))

    return set() if not set_word_lengths_subset.issubset(set_word_lengths_superset) else set_word_lengths_subset


def get_set_of_words_that_start_with_letter(letter, index, *args):
    """Retrieve a set of words that contain a target letter at a specified index.

    Parameters
    ----------
    letter : str
        Elements from provided sets will be filtered based on this parameter, which represents a target letter that is
        located at a specified index.
    index : int
        This parameter represents the location of a target letter.
    *args
        This parameter allows a variable number of sets to be passed as arguments so that separate collections of words
        can be filtered together.

    Returns
    -------
    set
        A set containing all of the words that have a given letter at a specified index will be returned.

    Raises
    ------
    TypeError
        This error is raised when the value of letter is not a string.
        This error is raised when the index is not an integer.
        This error is raised when an argument passed to *args is not a set.
    ValueError
        This error is raised when the value of letter does not have a length of one.
        This error is raised when the index is less than zero.
    """
    if not isinstance(letter, str):
        raise TypeError("The letter must be a string.")

    if len(letter) != 1:
        raise ValueError("The value of letter must have a length of one.")

    if not isinstance(index, int):
        raise TypeError("The index must be an integer.")

    if index < 0:
        raise ValueError("The value of index must be an unsigned integer.")

    s = set()

    for a in args:
        if type(a) != set:
            raise TypeError("Each element that is contained in the tuple of variable arguments must be a set.")

        s.update(a)

    uppercase_letter = letter.capitalize()

    return set(filter(lambda w: index < len(w) and w[index].capitalize() == uppercase_letter, s))
