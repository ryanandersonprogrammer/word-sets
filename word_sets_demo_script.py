# Copyright 2019 Ryan E. Anderson
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Word Sets Module Demo Script v1.0.0
#
# by Ryan E. Anderson
#
# Copyright (C) 2019 Ryan E. Anderson

"""Word Sets Module Demo Script

This script can be used to evaluate Word Sets Module.
"""
import word_sets_demo
import wordsets

print("")
print(wordsets.__doc__)
print("get_word_length_subset()")
print("------------------------")
print("")
print(wordsets.get_word_length_subset.__doc__)
print("get_set_of_words_that_start_with_letter()")
print("-----------------------------------------")
print("")
print(wordsets.get_set_of_words_that_start_with_letter.__doc__)
print("Demo")
print("----")
print("")

while True:
    list_superset = []
    list_set = []

    size_superset = int(input("(1) Enter the number of elements that belong to a superset of words: "))

    word_sets_demo.append_input_to_list_of_set_elements(size_superset, list_superset)

    size_set = int(input("(2) Enter the number of elements that belong to a set of words: "))

    word_sets_demo.append_input_to_list_of_set_elements(size_set, list_set)

    superset = set(map(lambda w: len(w), list_superset))
    subset_even = wordsets.get_word_length_subset(lambda w: w % 2 == 0, list_superset, list_set)
    subset_greater_than_three = wordsets.get_word_length_subset(lambda w: w > 3, list_superset, list_set)
    subset_less_than_four = wordsets.get_word_length_subset(word_sets_demo.is_less_than_four, list_superset, list_set)

    difference_even = word_sets_demo.get_difference_between_subset_and_superset(superset, subset_even)
    difference_greater_than_three = word_sets_demo.get_difference_between_subset_and_superset(superset,
                                                                                              subset_greater_than_three)
    difference_less_than_four = word_sets_demo.get_difference_between_subset_and_superset(superset,
                                                                                          subset_less_than_four)

    set_words_that_start_with_a_given_letter = wordsets.get_set_of_words_that_start_with_letter("a", 0, set(list_set),
                                                                                                set(list_superset))

    print(f"The following subset contains all word lengths that are even: {subset_even}.")
    print(f"The difference between the superset and this subset is as follows: {difference_even}.")
    print("")
    print(f"The following subset contains all word lengths that are greater than three: {subset_greater_than_three}.")
    print(f"The difference between the superset and this subset is as follows: {difference_greater_than_three}.")
    print("")
    print(f"The following subset contains all word lengths that are less than four: {subset_less_than_four}.")
    print(f"The difference between the superset and this subset is as follows: {difference_less_than_four}.")
    print("")
    print(f"The union of the sets that contain the elements from the provided lists has the following members that "
          f"start with \"a\": {set_words_that_start_with_a_given_letter}.")
    print("")

    code = input("Enter 0 to exit or any other key to continue: ")

    if code == "0":
        break

    print("")
